const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3001;
const api = require('./API');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(api);
app.listen(port, () => console.log(`Example app listening on port ${port}!`));