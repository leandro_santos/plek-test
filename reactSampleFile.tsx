import _ from 'lodash';
import moment from 'moment';
import * as React from 'react';
import styled from 'styled-components';
import { get } from '../../../common/services/HttpService';
import { borderColorLight, borderRadiusDefault, paddingButton, textColorLighter } from '../../../common/styles/_variables';
import { Loader } from '../shared/Loader';
import PlekIcon from '../shared/PlekIcon';
import Clickable from '../shared/generic/Clickable/GenericClickable';

const Wrapper = styled.div`
	background-color: white;
	border-radius: ${borderRadiusDefault};
	display: flex;
	flex-direction: column;
	margin-bottom: 30px;
	width: 100%;
`;

const Header = styled.header`
	border-bottom: 1px ${borderColorLight} solid;
	display: flex;
	align-items: center;
	padding: 15px;
	width: 100%;
	i {
		padding-right: 18px;
	}
`;

const Title = styled.h2`
	font-size: 18px;
	font-weight: 600;
	margin: 0;
	padding: 0;
	i {
		display: block;
	}
`;

const TitleSmall = styled.h3`
	font-size: 11px;
	font-weight: 600;
	margin: 0;
	padding: 0;
	i {
		display: block;
	}
`;

const ContentWrapper = styled.div`
	padding: 0 15px 15px 15px;
	> a {
		display: block;
		margin: 15px auto;
	}
`;

const LoaderWrapper = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	min-height: 150px;
`;

const List = styled.ul`
	margin: 0;
	padding: 0;
	list-style: none;
`;

const ListItem = styled.li`
	border-bottom: 1px ${borderColorLight} solid;
	padding: 15px 0;
`;

const Button = styled(Clickable)`
	background: black;
	border: 0;
	border-radius: ${borderRadiusDefault};
	color: white;
	display: inline-block;
	font-size: 14px;
	font-weight: 600;
	margin-bottom: 15px;
	padding: ${paddingButton};
	text-align: center;
	width: 140px;
	i {
		padding-left: 5px;
	}
	&:hover,
	&:focus {
		color: white;
		text-decoration: none;
	}
`;

const TermListItem = styled.li`
	display: inline-flex;
	margin-top: 5px;
	margin-right: 5px;
`;

const TermButton = styled(Clickable)`
	border-radius: ${borderRadiusDefault};
	color: white;
	font-size: 11px;
	padding: 5px;
	&:hover,
	&:focus {
		color: white;
		text-decoration: none;
	}
`;

interface IExploreWidgetProps {
	name: string;
	TranslateService: (string) => string;
	user: any;
	$location: any;
	$timeout: any;
}

interface IExploreWidgetState {
	isBusy: boolean;
	items: any;
	taxonomies: any;
}

class ExploreWidget extends React.Component<IExploreWidgetProps, IExploreWidgetState> {
	constructor(props) {
		super(props);

		this.state = {
			// initialise the component with isBusy: true
			isBusy: false,
			items: [],
			taxonomies: [],
		};
	}

	public render() {
		const { name, TranslateService, user } = this.props;
		const { isBusy, items, taxonomies } = this.state;
		// The first parameter of findIndex suggests that user object has a property 'role' which is an array:
		// user ={
		// 	...
		// 	'role': ['extrnal-access', 'internal-access']
		// }
		const isUserExternal = _.findIndex(user.role, function(o) {
			return o == 'external-access';
		});

		// Hide widget if user has role external-access
		if (isUserExternal === -1) {
			return (
				<Wrapper>
					<Header>
						<PlekIcon name="activity" size="extra-small" />
						<Title>{TranslateService(name)}</Title>
					</Header>
					<ContentWrapper>
						{items.length > 0 ? (
							<List>{this._renderItems(items)}</List>
						) : (
							isBusy && (
								<LoaderWrapper>
									<Loader size="small" />
								</LoaderWrapper>
							)
						)}
						<Button onClick="/explore" className="plek-background-color plek-background-color-hover-105">
							{TranslateService('EXPLORE')}
							<PlekIcon name="arrow-right" size="extra-small" />
						</Button>
						<TitleSmall>{TranslateService('OR_SELECT_A_TOPIC_TO_EXPLORE')}:</TitleSmall>
						{taxonomies.length > 0 ? (
							<List>{this._renderTaxonomyItems(taxonomies)}</List>
						) : (
							isBusy && (
								<LoaderWrapper>
									<Loader size="small" />
								</LoaderWrapper>
							)
						)}
					</ContentWrapper>
				</Wrapper>
			);
		} else {
			return null;
		}
	}

	public componentDidMount() {
		this.getItems();
	}

	public getItems = () => {
		const url = '/explorer/find';
		const params = {
			limit: 3,
			offset: 0,
		};

		get(url, undefined, params).then((res) => {
			this.setState({
				isBusy: false,
				items: res.suggestion,
				taxonomies: res.taxonomy,
			});
		});

		// I would not use it this way, because this is an anti-pattern: we should not nest setState functions
		this.setState(
			{
				isBusy: true,
			},
			() => {
				// if using like this, this has to be at least returned
				get(url, undefined, params).then((res) => {
					this.setState({
						isBusy: false,
						items: res.suggestion,
						taxonomies: res.taxonomy,
					});
				});
			}
		);
	};

	public _renderItems(items) {
		const language = _.get(window, 'userObj.language');

		return items.map((item) => {
			const modelType = _.get(item, 'modelType');

			// Author
			const author = _.get(item, 'user.fullname');
			const authorId = _.get(item, 'user.id');
			const authorProfileUrl = `/user/${authorId}`;

			// Message
			const messageUrl = `/message/${item.id}`;

			/// Group
			const group = _.get(item, 'name');
			const groupId = _.get(item, 'id');
			const groupUrl = `/group/${groupId}`;

			// Agenda
			const agendaId = _.get(item, 'id');
			const agendaUrl = `/agendaItem/${agendaId}`;

			switch (modelType) {
				case 'MESSAGE':
					return (
						<ListItem key={item.id}>
							<div style={{ color: textColorLighter, fontSize: 10 }}>
								<a style={{ color: textColorLighter, fontWeight: 600, paddingRight: 5 }} href={authorProfileUrl}>
									{author}
								</a>
								-
								<span style={{ paddingLeft: 5 }}>
									{moment(item.createdAt)
										.locale(language)
										.fromNow()}
								</span>
							</div>
							<div>
								<h3 style={{ fontSize: 13, fontWeight: 600, margin: 0, padding: 0 }}>
									<a href={messageUrl}>{item.subject}</a>
								</h3>
							</div>
						</ListItem>
					);
					break;
				case 'GROUP':
					return (
						<ListItem key={item.id}>
							<div style={{ color: textColorLighter, fontSize: 10 }}>
								<span>
									{moment(item.createdAt)
										.locale(language)
										.fromNow()}
								</span>
							</div>
							<div>
								<h3 style={{ fontSize: 13, fontWeight: 600, margin: 0, padding: 0 }}>
									<a href={groupUrl}>{group}</a>
								</h3>
							</div>
						</ListItem>
					);
					break;
				case 'AGENDA':
					return (
						<ListItem key={item.id}>
							<div style={{ color: textColorLighter, fontSize: 10 }}>
								<a style={{ color: textColorLighter, fontWeight: 600, paddingRight: 5 }} href={authorProfileUrl}>
									{author}
								</a>
								-
								<span style={{ paddingLeft: 5 }}>
									{moment(item.createdAt)
										.locale(language)
										.fromNow()}
								</span>
							</div>
							<div>
								<h3 style={{ fontSize: 13, fontWeight: 600, margin: 0, padding: 0 }}>
									<a href={agendaUrl}>{item.subject}</a>
								</h3>
							</div>
						</ListItem>
					);
					break;
			}
		});
	}

	public _renderTaxonomyItems(items) {
		const taxonomy = _.get(items, '[0]');
		const terms = taxonomy.terms || [];

		return terms.map((term: any, index: number) => {
			const indexNumber = index + 1;

			return (
				<TermListItem key={term.slug}>
					<TermButton
						style={{ backgroundColor: term.color }}
						className={!term.color ? 'plek-background-color plek-background-color-hover' : ''}
						onClick={() => this._onClickTerm(taxonomy.id, term.slug, indexNumber)}>
						{term.name}
					</TermButton>
				</TermListItem>
			);
		});
	}

	public _onClickTerm = (taxonomy, term, activeIndex) => {
		const { $location, $timeout } = this.props;

		$timeout(() => {
			$location.path('/explore').search({ taxonomy, term, activeIndex });
		});
	};
}

export default ExploreWidget;
