## React Technical Assessment


### Task 1
#### ReactSampleFile

If you look at the ReactSampleFile, what would you improve change in this code? Any bugs to expect when you look at this file?

### Task 2
#### Available resources
The backend has 3 resources, `posts`, `users` and `groups` and CRUD actions available.  
For example: http://localhost:3000/users?name=user1

#### Todo
1. A login form (just ask for user name)
2. Dashboard that show the posts ordered by time in separated blocks per group  
3. Implement a virtual block that shows the user who is having his/her birthday today
4. A user profile that shows current, previous and next birthday
5. CRUD on the entities (bonus)
6. Nice UI (bonus)  
7. Creative UI/UX (bonus)  



**Note**: The timing between each commit is important and the duration between git init and last commit should be less than 3 h. 

**Note** To do the assessment : 
1. Read this file carefully, if anything is not clear, please ask before starting. 
2. Create a private repository in gitlab.com, add [assessment-supervisor] to your repository. 
3. Put the first commit and push it in your git repo ( your technical assessment beginning ) you have 3 hours from this commit to finish the tasks. 


