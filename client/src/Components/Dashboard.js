import React, { useState, useEffect } from "react";
import axios from "axios";
import { Block } from "./Block";

const Dashboard = () => {
  const [blocks, setBlocks] = useState([]);

  useEffect(() => {
    const getPosts = async () => {
      const posts = await axios.get("/posts");

      const blocks = posts.data
        .reduce((acc, curr) => {
          if (curr.group in acc) {
            acc[curr.group].push(curr);
          } else {
            acc[curr.group] = [curr];
          }
          return acc;
        }, {})
        
        const sortedBlocks = Object.values(blocks).sort((a, b) => {
          if (a.group < b.group) {
            return -1;
          }
          if (a.group > b.group) {
            return 1;
          }
          return 0;
        });

      setBlocks(sortedBlocks);
    };
    getPosts();
  }, []);
 console.log('it works');
  return (

    <section>
      {blocks && blocks.map((block, i) => <Block posts={block} key={i} />)}
    </section>
  );
};

export default Dashboard;
