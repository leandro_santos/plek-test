import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import Jumbotron from 'react-bootstrap/Jumbotron';

const LoginForm = styled(Form)`
  width: 60vw;
  margin: 0 auto;
`;

const JumbotronForm = styled(Jumbotron)`
  background: #eaeae9;
  margin-top: 50px;
`

const LoginName = styled(Form.Label)`
  color: 		#000000;
  font-size: 20px;
`


function Login() {
  let history = useHistory();

  const onSubmit = event => {
    const form = event.currentTarget;
    if (!form.checkValidity()) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      history.push("/home");
    }
  };
  return (
    <JumbotronForm>
      <LoginForm action="#" onSubmit={onSubmit}>
        <Form.Group controlId="name">
          <LoginName>Login</LoginName>
          <Form.Control required type="name" placeholder="Enter name" />
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
      </LoginForm>
  </JumbotronForm>
    
  );
}

export default Login;
