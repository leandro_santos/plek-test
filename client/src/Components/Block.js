import React from "react";
import styled from "styled-components";
import Card from 'react-bootstrap/Card';

const BlockCard = styled(Card)`
  margin-top: 50px;
`

export const Block = ({ posts }) => (
  <BlockCard>
    {posts.map((post, i) => (
      <Card.Title key={i}>{post.title}</Card.Title>
    ))}
  </BlockCard>
);
