import React from "react";
import { Route, Switch } from "react-router-dom";
import styled from "styled-components";
import "./App.css";
import Dashboard from './Components/Dashboard';
import Login from './Components/Login';

const AppWrapper = styled.div`
  text-align: center;
  max-width: 960px;
  margin: 0 auto;
`;

const Routes = () => (
  <Switch>
    <Route path="/" exact component={Login} />
    <Route path="/home" exact component={Dashboard} />
  </Switch>
);

function App() {
  
  return (
    <AppWrapper>
      <Routes />
      
    </AppWrapper>
  );
}

export default App;
